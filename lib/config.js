var fs = require("fs");
var yaml = require("js-yaml");

var util = require("./util.js");

var config;

module.exports.get_config = function(cb) {
	if (typeof cb !== "function")
		throw new TypeError("callback must be a function");

	if (config) return cb(null, config);

	return fs.readFile(util.config_name, "utf8", read_config_file);

	function read_config_file(err, data) {
		if (err) {
			return console.err(err);
		}

		try {
			config = yaml.safeLoad(data);
		} catch (e) {
			return cb(e);
		}

		return config_checks(err);
	}

	function config_checks(err) {
		if (err) cb(err);

		for (var i = 0; i < config.watch_paths.length; i++) {
			if (!fs.existsSync(config.watch_paths[i])) {
				err = new Error(config.watch_paths[i] + " does not exist");
			}
		}

		return cb(err, config);
	}
}

module.exports.unload_config = function(cb) {
	if (typeof cb !== "function" && typeof cb !== "undefined")
		throw new TypeError("callback must be a function or undefined");

	config = null;
	if (cb) return cb(null, config);
}
