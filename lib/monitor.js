var fam = require('fam.js');

var config = require('./config.js');

var monitor = new fam.FAM();

module.exports.init = function(cb) {
	if (cb && typeof cb !== "function")
		throw new TypeError("callback must be a function");

	return config.get_config(on_config_load);

	function on_config_load(err, config) {
		if (err) {
			console.err("could not read config");
			return console.err(err);
		}

		try {
			config.watch_paths.forEach(load_monitor);
		} catch (e) {
			return cb(e);
		}

		return cb();
	}

	function load_monitor(value, index, array) {
		monitor.addPaths(value);
	}
}

module.exports.paths = function() {
	return monitor.paths;
}

module.exports.start_mon = function(cb) {
	monitor.watch(cb);
}
