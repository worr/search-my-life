var fs = require('fs');
var nodeunit = require("nodeunit");

var conf = require("../lib/config.js");

var test_config_values_values = "watch_paths:\n- /var/tmp\n- /tmp\nuse_net: true";
var test_config_file_presence_values = "watch_paths:\n- /if_this_file_exists_you_are_crazy\n";
var _rf = null;

function swap_readFile(contents) {
	if (_rf) {
		fs.readFile = _rf;
		_rf = null;

		return;
	}

	_rf = fs.readFile;
	fs.readFile = function(path, encoding, cb) {
		cb(null, contents);
	}
}

module.exports.test_config_values = function(test) {
	swap_readFile(test_config_values_values);
	conf.get_config(on_get_config);
	return swap_readFile();

	function on_get_config(err, config) {
		test.ifError(err);

		test.ok(config);
		test.equals(config.use_net, true, "testing use_net config value");
		test.deepEqual(config.watch_paths, [ "/var/tmp", "/tmp" ], "testing watch_paths value");
		conf.unload_config();
		test.done();
	}
}

module.exports.test_config_unload = function(test) {
	swap_readFile(test_config_values_values);
	conf.get_config(on_get_config);
	return swap_readFile();

	function on_get_config(err, config) {
		test.ifError(err);

		conf.unload_config(on_unload_config);
	}

	function on_unload_config(err, config) {
		test.ifError(err);

		test.equal(config, null, "testing if config was totally unloaded");
		test.done();
	}
}

module.exports.test_config_file_presence = function(test) {
	swap_readFile(test_config_file_presence_values);
	conf.get_config(on_get_config);
	return swap_readFile();

	function on_get_config(err, config) {
		test.deepEqual(err, new Error(config.watch_paths[0] + " does not exist"));

		test.done();

		conf.unload_config();
	}
}
