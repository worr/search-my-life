var fam = require('fam.js');
var fs = require('fs');
var nodeunit = require('nodeunit');

var config = require('../lib/config.js');
var monitor = require('../lib/monitor.js');

var _fr;
var mon = new fam.FAM();

module.exports.test_monitor_init = function(test) {
	return monitor.init(on_monitor_init);

	function on_monitor_init(err) {
		test.ifError(err);

		test.done();
	}
}

module.exports.test_monitor_paths = function(test) {
	return config.get_config(on_get_config);

	function on_get_config(err, config) {
		test.ifError(err);

		test.deepEqual(monitor.paths(), config.watch_paths, "comparing watched dirs and configure watch dirs");
		test.done();
	}
}

module.exports.setUp = function(cb) {
	_fr = fs.readFile;
	fs.readFile = function(path, encoding, cb) {
		return cb(null, "watch_paths:\n- /var/tmp\n- /tmp\nuse_net: true");
	}

	cb();
}

module.exports.tearDown = function(cb) {
	fs.readFile = _fr;

	cb();
}
